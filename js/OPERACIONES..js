// Obtenemos los elementos del DOM
const monedaOrigen = document.getElementById('moneda-origen');
const monedaDestino = document.getElementById('moneda-destino');
const cantidad = document.getElementById('cantidad');
const calcularBtn = document.getElementById('calcular');
const importePagar = document.getElementById('importe-pagar');

// Función para calcular el importe a pagar
function calcularImporte() {
	// Obtenemos los valores seleccionados por el usuario
	const origen = monedaOrigen.value;
	const destino = monedaDestino.value;
	const cantidadValor = cantidad.valueAsNumber;

	// Validamos que la cantidad sea mayor que cero
	if (cantidadValor <= 0) {
		alert('La cantidad debe ser mayor que cero');
		return;
	}

	// Obtenemos la tasa de conversión
	let tasa = 0;
	switch (origen) {
		case 'USD':
			switch (destino) {
				case 'MXN':
					tasa = 19.85;
					break;
				case 'CAD':
					tasa = 1.35;
					break;
				case 'EUR':
					tasa = 0.99;
					break;
				default:
					tasa = 1;
			}
			break;
		case 'MXN':
			switch (destino) {
				case 'USD':
					tasa = 1 / 19.85;
					break;
				case 'CAD':
					tasa = 1 / 14.77;
					break;
				case 'EUR':
					tasa = 1 / 22.58;
					break;
				default:
					tasa = 1;
			}
			break;
		case 'CAD':
			switch (destino) {
				case 'USD':
					tasa = 1 / 1.35;
					break;
				case 'MXN':
					tasa = 1 / 14.77 / 19.85;
					break;
				case 'EUR':
					tasa = 1 / 1.49;
					break;
				default:
					tasa = 1;
			}
			break;
		case 'EUR':
			switch (destino) {
				case 'USD':
					tasa = 1 / 0.99;
					break;
				case 'MXN':
					tasa = 1 / 22.58 / 19.85;
					break;
				case 'CAD':
					tasa = 1 / 1.49;
					break;
				default:
					tasa = 1;
			}
			break;
	}

	// Calculamos el importe a pagar
	const importe = cantidadValor * tasa * 1.03;

	// Mostramos el resultado
	importePagar.value = `$ ${importe.toFixed(2)}`;
}

// Asociamos la función al botón "Calcular"
calcularBtn.addEventListener('click', calcularImporte);